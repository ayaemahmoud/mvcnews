//
//  Strings+Extentions.swift
//  MVCNews
//
//  Created by Aya E  Mahmoud on 12/9/19.
//  Copyright © 2019 Aya E  Mahmoud. All rights reserved.
//

import Foundation


extension String {
    
    var isBlank: Bool {
        return self.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty
    }
    
    var isEmail: Bool {
        let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,20}"
        let emailTest  = NSPredicate(format:"SELF MATCHES %@", emailRegex)
        return emailTest.evaluate(with: self)
    }
    
    var isPasswordMoreThanTenChars: Bool {
        if self.count > 9 {
            return true
        } else {
            return false
        }
    }
}
