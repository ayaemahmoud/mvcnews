//
//  NewsCell.swift
//  MVCNews
//
//  Created by Aya E  Mahmoud on 9/18/19.
//  Copyright © 2019 Aya E  Mahmoud. All rights reserved.
//

import UIKit

class NewsCell: UITableViewCell {

    @IBOutlet weak var newsUrl: UILabel!
    @IBOutlet weak var newsHeading: UILabel!
    @IBOutlet weak var newsDescription: UILabel!
    @IBOutlet weak var newsImageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
