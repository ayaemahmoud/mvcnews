//
//  CustomTextFeild.swift
//  MVCNews
//
//  Created by Aya E  Mahmoud on 11/30/19.
//  Copyright © 2019 Aya E  Mahmoud. All rights reserved.
//

import UIKit
import TweeTextField
import UIKit

class CustomTextFeild: UIView {
    
    @IBOutlet weak var textField: TweeAttributedTextField!
    let nibName = "CustomTextField"
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        guard let view = loadViewFromNib() else { return }
        view.frame = self.bounds
        self.addSubview(view)
    }
    
    func loadViewFromNib() -> UIView? {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: nibName, bundle: bundle)
        return nib.instantiate(withOwner: self, options: nil).first as? UIView
    }
    
    @IBInspectable
    public var title: String = "" {
        didSet {
            self.textField.text = self.title
        }
    }
    
    @IBInspectable
    public var placeHolder: String = "" {
        didSet {
            self.textField.tweePlaceholder = self.placeHolder
            
        }
    }
    
//    public var errorMessage: String = "" {
//       didSet {
//        if !errorMessage.isBlank {
//            self.textField.activeLineColor = .red
//        } else {
//         
//            self.textField.activeLineColor =  colorLiteral(red: 0.1254901961, green: 0.8431372549, blue: 0.4078431373, alpha: 1)
//        }
//        
//        self.textField.showInfo(errorMessage, animated: true)
//    }
//        
//    }
}
