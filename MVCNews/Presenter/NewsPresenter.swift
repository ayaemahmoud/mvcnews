//
//  NewsPresenter.swift
//  MVCNews
//
//  Created by Aya on 1/5/20.
//  Copyright © 2020 Aya E  Mahmoud. All rights reserved.
//

import UIKit

class NewsPresenter {
    weak var view: NewsView?
    var page = 1
    var articles = [Article]()
    func attatchView(view: NewsView) {
        self.view = view
    }

    func getNews() {
        let newsRequest = NewsRequest(countryCode:"US", pageSize: 10, page: page)
        NewsService.getNews(newsRequest: newsRequest) { (articles, error) in
            self.view?.stopLoading()

            if error == nil {
                if self.page == 1 {
                    self.articles = articles
                } else {
                    self.articles.append(contentsOf: articles)
                }
                self.view?.onSucessLoadingNews(news: self.articles)
            } else {
                self.view?.onError(error: error.debugDescription)
            }
        }
    }
    
    func initPullToRefresh() {
        self.page = 1
        self.getNews()
    }
    
    func initLoadMore(){
        self.page += 1
        self.getNews()
    }
}
