//
//  NewsView.swift
//  MVCNews
//
//  Created by Aya on 1/5/20.
//  Copyright © 2020 Aya E  Mahmoud. All rights reserved.
//

import UIKit


protocol NewsView: class {
    func startWindless()
    func stopLoading()
    func onError(error: String)
    func onSucessLoadingNews(news:[Article])
}
