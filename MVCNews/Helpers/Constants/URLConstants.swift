//
//  URLConstants.swift
//  News-MVVM
//
//  Created by Mohamed Shaat on 7/27/19.
//  Copyright © 2019 shaat. All rights reserved.
//

struct APPURL {
    
    private struct Domains {
        static let Dev = "https://newsapi.org"
    }
    
    private struct Routes {
        static let Api = "" // for example api
    }
    
    private  struct Versions {
        static let Version1 = "/v1" // for example V1
        static let Version2 = "/v2" // for example V2
    }
    
    private static let Domain = Domains.Dev
    private static let Version = Versions.Version2
    private static let Route = Routes.Api
 
    public static var BaseURL: String {
        return  APPURL.Domain + APPURL.Route + APPURL.Version
    }
    
    public static var LoginURL: String {
           return "http:// /login"
    }
    
    struct Paths {
        static let NewsUrl  =  "/top-headlines"
        static let login = "login"
    }
    
}
