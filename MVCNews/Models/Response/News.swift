//
//  News.swift
//
//  Created by Aya E  Mahmoud on 9/18/19
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper

public final class News: Mappable, NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let status = "status"
    static let totalResults = "totalResults"
    static let articles = "articles"
  }

  // MARK: Properties
  public var status: String?
  public var totalResults: Int?
  public var articles: [Article]?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    status <- map[SerializationKeys.status]
    totalResults <- map[SerializationKeys.totalResults]
    articles <- map[SerializationKeys.articles]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = status { dictionary[SerializationKeys.status] = value }
    if let value = totalResults { dictionary[SerializationKeys.totalResults] = value }
    if let value = articles { dictionary[SerializationKeys.articles] = value.map { $0.dictionaryRepresentation() } }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.status = aDecoder.decodeObject(forKey: SerializationKeys.status) as? String
    self.totalResults = aDecoder.decodeObject(forKey: SerializationKeys.totalResults) as? Int
    self.articles = aDecoder.decodeObject(forKey: SerializationKeys.articles) as? [Article]
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(status, forKey: SerializationKeys.status)
    aCoder.encode(totalResults, forKey: SerializationKeys.totalResults)
    aCoder.encode(articles, forKey: SerializationKeys.articles)
  }

}
