//
//  DetailsViewController.swift
//  MVCNews
//

import UIKit

class DetailsViewController: UIViewController {
    
    @IBOutlet weak var titleLable: UILabel!
    @IBOutlet weak var descriptionLable: UILabel!
    var article: Article?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let article = article {
            titleLable.text = article.title
            descriptionLable.text = article.descriptionValue
        }
        
        // Do any additional setup after loading the view.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
