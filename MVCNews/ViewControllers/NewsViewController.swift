//
//  ViewController.swift
//  MVCNews
//
//  Created by Aya E  Mahmoud on 9/12/19.
//  Copyright © 2019 Aya E  Mahmoud. All rights reserved.
//

import UIKit
import Kingfisher
import PullToRefreshKit
import Windless

class NewsViewController: UIViewController {
    @IBOutlet weak var newsTableView: UITableView!
    var articles = [Article]()
    var windelssCount = 10;
    let presenter = NewsPresenter()
    override func viewDidLoad() {
        super.viewDidLoad()
        registerCell()
        initPullToRefresh()
        initLoadMore()
        presenter.attatchView(view: self)
        presenter.getNews()
        // Do any additional setup after loading the view, typically from a nib.
    }

    func registerCell()
    {
        newsTableView.register(UINib(nibName: "NewsCell", bundle: nil), forCellReuseIdentifier: "NewsCell")
    }
    
    func initPullToRefresh() {
        self.newsTableView.configRefreshHeader(container: self) {
            self.presenter.initPullToRefresh()
        }
    }
    
    func initLoadMore() {
        self.newsTableView.configRefreshFooter(container: self) {
            self.presenter.initLoadMore()
        }
    }
}

extension NewsViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if windelssCount != 0 {
            return windelssCount
        }
        return articles.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NewsCell", for: indexPath) as? NewsCell
        if articles.count > 0 {
            let artical = articles[indexPath.row]
            cell?.newsHeading.text = artical.title
            cell?.newsDescription.text = artical.descriptionValue
            cell?.newsUrl.text = artical.url
            if let imageString = artical.urlToImage, let url = URL(string:imageString) {
                cell?.newsImageView.kf.setImage(with: url)
            }
        }
        return cell!
    }
}


extension NewsViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 200.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if articles.count > 0 {
            let newDetail = articles[indexPath.row]
            self.performSegue(withIdentifier: "newsdetails", sender: newDetail)
        }
    }
}

extension NewsViewController: NewsView {
  
    func startWindless() {
        windelssCount = 10
        self.newsTableView.windless
                   .apply {
                       $0.beginTime = 0
                       $0.pauseDuration = 1
                       $0.duration = 1.5
                       $0.animationLayerOpacity = 0.8
                   }
                   .start()
    }
    
    func stopLoading() {
        self.newsTableView.switchRefreshFooter(to: .normal)
        self.newsTableView.switchRefreshHeader(to: .normal(.success,0.0))
        self.newsTableView.windless.end()
        self.windelssCount = 0
        self.newsTableView.reloadData()
    }
    
    func onSucessLoadingNews(news: [Article]) {
        self.articles = news
        self.newsTableView.reloadData()
        
    }
    
    func onError(error: String){
        let alert = UIAlertController(title: "", message: error, preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: .default, handler: {
            action in
        })
        alert.addAction(action)
        self.present(alert, animated: true, completion: nil)
    }
    
}
