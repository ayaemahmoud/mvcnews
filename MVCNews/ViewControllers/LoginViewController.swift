//
//  LoginViewController.swift
//  MVCNews
//
//  Created by Aya E  Mahmoud on 11/18/19.
//  Copyright © 2019 Aya E  Mahmoud. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import TransitionButton

class LoginViewController: UIViewController {

    @IBOutlet weak var passwordTextFeild: CustomTextFeild!
    @IBOutlet weak var emailTextField: CustomTextFeild!
    @IBOutlet weak var loginBtn: TransitionButton!

    var animationLoginBtn = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        IQKeyboardManager.shared.enable = true
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        self.view.addGestureRecognizer(tap)
        
        setupTextFeilds()
        // Do any additional setup after loading the view.
    }
    
    @objc func handleTap(_ sender: UITapGestureRecognizer? = nil) {
        if animationLoginBtn == true {
            loginBtn.stopAnimation()
            animationLoginBtn = false
        }
    }

    @IBAction func Login(_ sender: Any) {
        if vaildateTextFileds(){
            loginBtn.startAnimation()
        }
    }

    func setupTextFeilds(){
        
        emailTextField.textField.keyboardType = .emailAddress
        emailTextField.textField.returnKeyType = .next
        emailTextField.textField.delegate = self
        
        passwordTextFeild.textField.isSecureTextEntry = true
        passwordTextFeild.textField.returnKeyType = .done
        passwordTextFeild.textField.delegate = self
    }
    
     func vaildateTextFileds() -> Bool {
         var valid = true
         if !emailTextField.textField.text!.isBlank {
             if !emailTextField.textField.text!.isEmail {
                 valid = false
                emailTextField.textField.showInfo("Wrong Email")
             }
         } else {
             valid = false
            emailTextField.textField.showInfo("iS Empty")
         }
         if !passwordTextFeild.textField.text!.isBlank {
             if !passwordTextFeild.textField.text!.isPasswordMoreThanTenChars {
                 valid = false
                passwordTextFeild.textField.showInfo("in Valid Password")
             }
         } else {
             valid = false
            passwordTextFeild.textField.showInfo("iS Empty")
         }
         return valid
     }
}


extension LoginViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == emailTextField.textField {
            passwordTextFeild.textField.becomeFirstResponder()
        }else{
            self.view.endEditing(true)
        }
        
        return true
    }
}
