//
//  NewsService.swift
//  MVCNews
//
//  Created by Mohamed Shaat on 9/19/19.
//  Copyright © 2019 Aya E  Mahmoud. All rights reserved.
//

import Foundation
import Moya
import ObjectMapper

class NewsService {
    
    class func getNews(newsRequest:NewsRequest,
                       completionHandler: @escaping ([Article] , Error?)-> Void) {
        
        let provider = MoyaProvider<HeadlinesApi>()
        provider.request(.getTopHeadlines(newsRequest: newsRequest), completion: { result in
            switch result {
            case let .success(newsResponse):
                do {
                    newsResponse.statusCode
                    print(try newsResponse.mapJSON() as! [String : Any])
                    let news = Mapper<News>().map(JSON:try newsResponse.mapJSON() as! [String : Any])
                    completionHandler(news?.articles ?? [], nil)
                } catch {
                    completionHandler([], error)
                }
                
                break
            case .failure(let error):
                completionHandler([], error)
                break
            }
        })
    }
}
