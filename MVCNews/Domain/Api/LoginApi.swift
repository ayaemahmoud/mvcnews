//
//  LoginApi.swift
//  MVCNews
//
//  Created by Aya on 12/9/19.
//  Copyright © 2019 Aya E  Mahmoud. All rights reserved.
//

import Foundation
import Moya

enum LoginApi {
    
}

extension LoginApi: TargetType {
    
    var baseURL: URL {
        return URL(string: APPURL.LoginURL)!
    }
    
    var path: String {
        return APPURL.Paths.login
    }
    
    var method: Moya.Method {
        return .post
    }
    
    var sampleData: Data {
        return Data()
    }
    
    var task: Task {
        <#code#>
    }
    
    var headers: [String : String]? {
        return [Key.Headers.ContentType: Key.Headers.KEY_ContentTypeValue]
    }
    
}
